import type { App, Plugin } from 'vue';

export type SFCWithInstall<T> = T &
  Plugin & {
    displayName?: string;
    name: string;
    install: {
      (app: App): void;
    };
  };

export const withInstall = <T>(component: T): SFCWithInstall<T> => {
  const c = component as any;
  c.install = (app: App) => {
    app.component(c.name, c);
  };

  return c;
};
