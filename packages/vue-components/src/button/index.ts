import { withInstall } from '../util/install';
import Button from './button.vue';

Button.name = 'ZButton';

export default withInstall(Button);
